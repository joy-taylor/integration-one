import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          GO1 Integration Test Site
        </p>
        <a
          className="App-link"
          href="https://www.go1.com/en-au/developers/partners/concepts/getting-started"
          target="_blank"
          rel="noopener noreferrer"
        >
          GO1 Developer - Integration - Getting Started
        </a>
      </header>
    </div>
  );
}

export default App;
